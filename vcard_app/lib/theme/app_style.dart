import 'package:flutter/material.dart';

import '../utils/color_constant.dart';
import '../utils/size_utils.dart';

class AppStyle {
  static TextStyle txtMontserratRomanSemiBold32 = TextStyle(
    color: ColorConstant.indigo900,
    fontSize: getFontSize(
      28,
    ),
    fontFamily: 'Montserrat',
    fontWeight: FontWeight.w600,
  );

  static TextStyle txtRobotoRegular16 = TextStyle(
    color: ColorConstant.bluegray400,
    fontSize: getFontSize(
      16,
    ),
    fontFamily: 'Roboto',
    fontWeight: FontWeight.w400,
  );

  static TextStyle txtRobotoRomanRegular20 = TextStyle(
    color: ColorConstant.gray700,
    fontSize: getFontSize(
      24,
    ),
    fontFamily: 'Roboto',
    fontWeight: FontWeight.w400,
  );

  static TextStyle txtRobotoRomanMedium16 = TextStyle(
    color: ColorConstant.gray700,
    fontSize: getFontSize(
      16,
    ),
    fontFamily: 'Roboto',
    fontWeight: FontWeight.w500,
  );

  static TextStyle txtRobotoRegular20 = TextStyle(
    color: ColorConstant.black900,
    fontSize: getFontSize(
      20,
    ),
    fontFamily: 'Roboto',
    fontWeight: FontWeight.w400,
  );
  static TextStyle txtRobotoRegular40 = TextStyle(
    color: ColorConstant.black900,
    fontSize: getFontSize(
      40,
    ),
    fontFamily: 'Roboto',
    fontWeight: FontWeight.w400,
  );
}
