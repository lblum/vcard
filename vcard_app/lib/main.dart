@JS()
library lib;

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_web_plugins/url_strategy.dart';

import 'package:holando_vcard/models/persona.dart';
import 'package:holando_vcard/ui/error_page.dart';
import 'package:holando_vcard/ui/vcard_page.dart';
import 'package:js/js.dart';

@JS()
external String globalPersona;

main() async {
  usePathUrlStrategy();
  WidgetsFlutterBinding.ensureInitialized();

  await dotenv.load(fileName: ".env");
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
  ]);

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  /*
  @override
  Widget build(context) {
    String pStr = globalPersona.replaceAll('\r', '\\r').replaceAll('\n', '\\n');
    try {
      return VCardPage(Persona.fromJson(jsonDecode(pStr)));
    } catch (e) {
      return ErrorPage();
    }
  }
  */

  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'La Holando Tarjeta virtual',
      //home: PWAVerticalPage(),
      //initialRoute: '/?${this.hash}',
      debugShowCheckedModeBanner: false,
      onGenerateRoute: (settings) {
        String pStr =
            globalPersona.replaceAll('\r', '\\r').replaceAll('\n', '\\n');
        return MaterialPageRoute(builder: (context) {
          try {
            return VCardPage(Persona.fromJson(jsonDecode(pStr)));
          } catch (e) {
            return ErrorPage();
          }
        });
      },
      onUnknownRoute: (RouteSettings settings) {
        return MaterialPageRoute<void>(
            settings: settings,
            builder: (BuildContext context) {
              return ErrorPage();
            });
      },
    );
  }
}
