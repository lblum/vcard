import 'package:json_annotation/json_annotation.dart';

part 'persona.g.dart';

@JsonSerializable()
class Persona {
  Persona(
      this.id,
      this.x_hash,
      this.n_legajo,
      this.x_nombres,
      this.x_apellidos,
      this.x_email,
      this.x_sector,
      this.x_cargo,
      this.x_movil,
      this.x_telefono,
      this.x_direccion,
      this.x_linkedin,
//this.created_at,
//this.updated_at,
      this.x_vcard,
      this.x_vcard_url,
      this.x_image_qr,
      this.x_vcard_img);

  String id;
  String x_hash;
  String n_legajo;
  String x_nombres;
  String x_apellidos;
  String x_email;
  String? x_sector;
  String x_cargo;
  String x_movil;
  String x_telefono;
  String x_direccion;
  String x_linkedin;
  //Date created_at;
  //Date updated_at;
  String x_vcard;
  String x_vcard_url;
  String x_image_qr;
  String x_vcard_img = '';

  factory Persona.fromJson(Map<String, dynamic> json) =>
      _$PersonaFromJson(json);

  Map<String, dynamic> toJson() => _$PersonaToJson(this);
}
