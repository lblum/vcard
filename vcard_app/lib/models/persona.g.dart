// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'persona.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Persona _$PersonaFromJson(Map<String, dynamic> json) => Persona(
      json['id'] as String,
      json['x_hash'] as String,
      json['n_legajo'] as String,
      json['x_nombres'] as String,
      json['x_apellidos'] as String,
      json['x_email'] as String,
      json['x_sector'] as String?,
      json['x_cargo'] as String,
      json['x_movil'] as String,
      json['x_telefono'] as String,
      json['x_direccion'] as String,
      json['x_linkedin'] as String,
      json['x_vcard'] as String,
      json['x_vcard_url'] as String,
      json['x_image_qr'] as String,
      json['x_vcard_img'] as String,
    );

Map<String, dynamic> _$PersonaToJson(Persona instance) => <String, dynamic>{
      'id': instance.id,
      'x_hash': instance.x_hash,
      'n_legajo': instance.n_legajo,
      'x_nombres': instance.x_nombres,
      'x_apellidos': instance.x_apellidos,
      'x_email': instance.x_email,
      'x_sector': instance.x_sector,
      'x_cargo': instance.x_cargo,
      'x_movil': instance.x_movil,
      'x_telefono': instance.x_telefono,
      'x_direccion': instance.x_direccion,
      'x_linkedin': instance.x_linkedin,
      'x_vcard': instance.x_vcard,
      'x_vcard_url': instance.x_vcard_url,
      'x_image_qr': instance.x_image_qr,
      'x_vcard_img': instance.x_vcard_img,
    };
