import 'package:flutter/material.dart';

import '../theme/app_style.dart';
import '../utils/image_constant.dart';

class ErrorPage extends StatelessWidget {
  const ErrorPage();

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Expanded(
            flex: 3,
            child: Center(
                child: Text(
              "algo no salió cómo esperaba...",
              overflow: TextOverflow.ellipsis,
              textAlign: TextAlign.center,
              style: AppStyle.txtRobotoRegular20,
            ))),
        Expanded(
            flex: 7,
            child: Center(
                child: Image.asset(
              ImageConstant.imgFlorkcalculospng,
            )))
      ],
    );
  }
}
