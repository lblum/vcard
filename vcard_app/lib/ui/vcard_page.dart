import 'package:flutter/material.dart';
import 'package:share_plus/share_plus.dart';
import 'package:url_launcher/url_launcher.dart';

import '../models/persona.dart';
import '../theme/app_decoration.dart';
import '../theme/app_style.dart';
import '../utils/color_constant.dart';
import '../utils/config.dart';
import '../utils/image_constant.dart';
import '../utils/size_utils.dart';
import '../widgets/custom_bottom_bar.dart';
import '../widgets/custom_image_view.dart';
import '../widgets/fab_widget.dart';

class VCardPage extends StatelessWidget {
  final Persona persona;
  const VCardPage(this.persona);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          width: double.maxFinite,
          decoration: AppDecoration.fillWhiteA700,
          child: Column(
            children: [
              Container(
                padding: getPadding(
                  left: 24,
                  top: 16,
                  right: 24,
                  bottom: 16,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    InkWell(
                      onTap: () {
                        launchUrl(Uri.parse(Config.homeURL));
                      },
                      child: Container(
                        width: double.maxFinite,
                        child: Container(
                          margin: getMargin(
                            top: 32,
                          ),
                          padding: getPadding(
                            left: 90,
                            top: 30,
                            right: 90,
                            bottom: 30,
                          ),
                          decoration: AppDecoration.outlineBlack9007f.copyWith(
                            borderRadius: BorderRadiusStyle.roundedBorder16,
                          ),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              CustomImageView(
                                svgPath: ImageConstant.imgGroup13,
                                height: getVerticalSize(
                                  155,
                                ),
                                width: getHorizontalSize(
                                  132,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: getPadding(
                        top: 24,
                      ),
                      child: Text(
                        '${persona.x_nombres} ${persona.x_apellidos}',
                        textAlign: TextAlign.center,
                        style: AppStyle.txtMontserratRomanSemiBold32,
                      ),
                    ),
                    Text(
                      '${persona.x_sector ?? ''} ${persona.x_cargo}',
                      textAlign: TextAlign.center,
                      style: AppStyle.txtRobotoRomanRegular20,
                    ),
                    Container(
                      margin: getMargin(
                        left: 64,
                        top: 41,
                        right: 64,
                      ),
                      decoration: AppDecoration.outlineGray70026.copyWith(
                        borderRadius: BorderRadiusStyle.roundedBorder8,
                      ),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          CustomImageView(
                            url: persona.x_vcard_img,
                            height: getSize(
                              184,
                            ),
                            width: getSize(
                              184,
                            ),
                          ),
                          Padding(
                            padding: getPadding(
                              top: 3,
                              bottom: 10,
                            ),
                            child: Text(
                              "+ Info",
                              overflow: TextOverflow.ellipsis,
                              textAlign: TextAlign.left,
                              style: AppStyle.txtRobotoRomanMedium16,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          )),
      bottomNavigationBar: CustomBottomBar(
        persona: persona,
      ),
      floatingActionButton: Fab(
          isRectangle: true,
          iconData: Icons.share,
          backGroundColor: ColorConstant.indigo900,
          onPressed: () {
            var shareText =
                '${Config.shareText} ${Config.baseURL}/${persona.x_hash}';
            Share.share(shareText,
                subject:
                    '${Config.shareSubject} ${persona.x_nombres} ${persona.x_apellidos}');
          },
          size: 56),
      floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
    );
  }
}
