import 'package:flutter/material.dart';

import '../utils/color_constant.dart';
import '../utils/size_utils.dart';

// ignore: must_be_immutable
class Fab extends StatelessWidget {
  final double size;
  Color? color;
  Color? backGroundColor;
  final iconData;
  final VoidCallback? onPressed;
  bool isRectangle;

  Fab(
      {this.size = 42,
      this.color,
      this.backGroundColor,
      required this.iconData,
      this.onPressed,
      this.isRectangle = true});

  @override
  Widget build(BuildContext context) {
    if (this.color == null) this.color = ColorConstant.blue600;
    if (this.backGroundColor == null)
      this.backGroundColor = ColorConstant.gray50;
    return SizedBox(
      width: getSize(this.size),
      height: getSize(this.size),
      child: FittedBox(
        child: FloatingActionButton(
          onPressed: this.onPressed ?? () => {},
          child: this.iconData is IconData
              ? Icon(
                  this.iconData,
                  color: this.color,
                )
              : this.iconData,
          backgroundColor: this.backGroundColor,
          shape: this.isRectangle
              ? RoundedRectangleBorder(
                  borderRadius:
                      BorderRadius.all(Radius.circular(getSize(20.0))))
              : null,
        ),
      ),
    );
  }
}
