import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

import '../models/persona.dart';
import '../utils/color_constant.dart';
import '../utils/config.dart';
import 'fab_widget.dart';

class CustomBottomBar extends StatelessWidget {
  final Persona persona;

  const CustomBottomBar({required this.persona});

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      //bottom navigation bar on scaffold
      color: ColorConstant.blue600,
      //shape: CircularNotchedRectangle(), //shape of notch
      padding: EdgeInsets.only(top: 10, bottom: 10),
      //notchMargin: 5, //notche margin between floating button and bottom appbar
      child: Row(
        //children inside bottom appbar
        //mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          // LLamar
          if (persona.x_movil != '' || persona.x_telefono != '')
            Fab(
              iconData: Icons.call,
              onPressed: () {
                var phone = (persona.x_movil != '')
                    ? persona.x_movil
                    : persona.x_telefono;
                phone = phone.replaceAll(new RegExp(r"\D"), "");
                var url = "tel:+${phone}";

                launchUrl(Uri.parse(url));
              },
            ),

          // WhatsApp
          if (persona.x_movil != '') SizedBox(width: 10),
          if (persona.x_movil != '')
            Fab(
              iconData: FontAwesomeIcons.whatsapp,
              onPressed: () {
                var phone = persona.x_movil.replaceAll(new RegExp(r"\D"), "");
                var msg = '';
                var url = "https://wa.me/${phone}/?text=${msg}";

                launchUrl(Uri.parse(url));
              },
            ),
          // email
          SizedBox(width: 10),
          Fab(
            iconData: Icons.email,
            onPressed: () {
              var url = 'mailto:${persona.x_email}';
              launchUrl(Uri.parse(url));
            },
          ),
          // Linkedin
          /*Fab(
            iconData: FontAwesomeIcons.linkedin,
            onPressed: () {
              launchUrl(Uri.parse(persona.x_linkedin));
            },
          ),*/
          // Descargar el contacto
          SizedBox(width: 10),
          Fab(
            iconData: Icons.add,
            onPressed: () {
              var url = '${Config.apiURL}/vcard-vcf/${persona.x_hash}';
              launchUrl(Uri.parse(url));
            },
          ),
        ],
      ),
    );
  }
}
