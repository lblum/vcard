class ImageConstant {
  static String imgBotonUbicacion = 'assets/images/img_ubicacion.svg';
  static String imgBotonLlamar = 'assets/images/img_llamar.svg';
  static String imgBotonWA = 'assets/images/img_WA.svg';
  static String imgBotonEmail = 'assets/images/img_email.svg';
  static String imgBotonLinkedin = 'assets/images/img_linkedin.svg';
  static String imgBotonDownload = 'assets/images/img_download.svg';
  static String imgBotonShare = 'assets/images/img_share.svg';
  static String imgFlorkcalculospng = 'assets/images/img_florkcalculospng.png';

  static String imgGroup13 = 'assets/images/img_group13.svg';

  static String imgImage7 = 'assets/images/img_image7.png';

  static String imgCut = 'assets/images/img_cut.svg';

  static String imageNotFound = 'assets/images/image_not_found.png';
}
