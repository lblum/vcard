import 'package:flutter_dotenv/flutter_dotenv.dart';

class Config {
  Config._();
  static var apiURL = dotenv.env['API_URL'] ?? "";
  static var baseURL = dotenv.env['BASE_URL'] ?? "";
  static var homeURL = dotenv.env['HOME_URL'] ?? "";
  static var testHash = dotenv.env['TEST_HASH'] ?? "";
  static var shareText = dotenv.env['SHARE_TEXT'] ?? "";
  static var shareSubject = dotenv.env['SHARE_SUBJECT'] ?? "";
}
