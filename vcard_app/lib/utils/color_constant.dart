import 'dart:ui';
import 'package:flutter/material.dart';

class ColorConstant {
  static Color black9007f = fromHex('#33000000');

  static Color gray700 = fromHex('#5f5f5f');

  static Color gray70026 = fromHex('#265f5f5f');

  static Color black900 = fromHex('#000000');

  static Color bluegray400 = fromHex('#888888');

  static Color blue600 = fromHex('#219cdc');

  static Color blue60003 = fromHex('#03219cdc');

  static Color blue60000 = fromHex('#00219cdc');

  static Color indigo900 = fromHex('#0c3071');

  static Color black9003f = fromHex('#3f000000');

  static Color whiteA700 = fromHex('#ffffff');

  static Color gray50 = fromHex('#fafafa');

  static Color fromHex(String hexString) {
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }
}
