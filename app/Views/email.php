<html>

<head>
    <style>
        .center {
            border: 5px solid;
            margin: auto;
            width: 50%;
            padding: 10px;
        }
    </style>
</head>

<body>
    <table border="0">
    <tr>
            <td style="text-align: center;">
            <img src="<?= base_url() ?>/img/isologo-tarjetas-full-color-200x200.png" />
            </td>
        </tr>
        <tr>
            <td>
                <p>Hola <?= $persona ?>!</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>Te enviamos el siguiente link para que puedas descargar tu tarjeta personal:</p>
            </td>
        </tr>
        <tr>
            <td>
                <a href="<?= $link ?>"><?= $link ?></a>
            </td>
        </tr>
        <tr>
            <td>
                <p>Al acceder al mismo, tendrás que hacer click en los tres puntitos/grilla desplegable y presionar el botón "Agregar a la página principal". Al confirmar se guardará el ícono en tu celular, al cual podrás acceder cuando lo desees.</p>
                <p>Saludos</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>Recursos Humanos</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>La Holando Sudamericana Cía. de Seguros S.A.</p>
            </td>
        </tr>
    </table>
</body>

</html>