<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Sector extends Migration
{
    public function up()
    {
        $fields = [
            'x_sector'          => ['type' =>  'text', 'null' => true,],
        ];

        $this->forge->addColumn('tnomina', $fields);
    }

    public function down()
    {
        $this->forge->dropColumn('tnomina', 'x_sector');

    }
}
