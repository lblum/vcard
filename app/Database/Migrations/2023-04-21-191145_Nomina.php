<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Nomina extends Migration
{
    public function up()
    {
        $this
            ->forge
            ->addField([
                'id'                => ['type' =>  'int', 'auto_increment' => true],
                'x_hash'            => ['type' =>  'varchar', 'constraint' => 1024, 'null' => true],
                'n_legajo'          => ['type' =>  'varchar', 'constraint' => 1024, 'null' => false],
                'x_nombres'         => ['type' =>  'text', 'null' => true],
                'x_apellidos'       => ['type' =>  'text', 'null' => true],
                'x_email'           => ['type' =>  'text', 'null' => true],
                'x_cargo'           => ['type' =>  'text', 'null' => true],
                'x_movil'           => ['type' =>  'text', 'null' => true],
                'x_telefono'        => ['type' =>  'text', 'null' => true],
                'x_direccion'       => ['type' =>  'text', 'null' => true],
                'x_linkedin'        => ['type' =>  'text', 'null' => true],
                'x_direccion'       => ['type' =>  'text', 'null' => true],
                'created_at'        => ['type' => 'datetime', 'null' => false],
                'updated_at'        => ['type' => 'datetime', 'null' => false],
            ]);

        $this
            ->forge
            ->addKey('id', true);
        $this
            ->forge
            ->addUniqueKey('n_legajo', 'inomina_n_legajo');
        $this
            ->forge
            ->addUniqueKey('x_hash', 'inomina_x_hash');

        $this
            ->forge
            ->createTable('tnomina', false, ['ENGINE' => 'InnoDB']);
    }

    public function down()
    {
        $this
            ->forge
            ->dropTable('tnomina');
    }
}
