<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class EmailSent extends Migration
{
    public function up()
    {
        $fields = [
            'm_sent'          => ['type' =>  'varchar', 'constraint' => 1, 'null' => false, 'default' =>'N'],
        ];

        $this->forge->addColumn('tnomina', $fields);

    }

    public function down()
    {
        $this->forge->dropColumn('tnomina', 'm_sent');
    }
}
