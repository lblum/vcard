<?php

namespace Config;

use CodeIgniter\Config\BaseConfig;

class VCard extends BaseConfig
{
    /**
     * El nombre de la empresa
     *
     * @var string
     */
    public $companyName= '';

    /**
     * la URL de la empresa
     *
     * @var string
     */
    public $companyURL= '';

    /**
     * Cuando se envían los mails, no se tiene la url base
     *
     * @var string
     */
    public $baseAppURL = 'https://qrtarjetas.laholando.com';

    public $mailFrom = 'lblum@menhir.com.ar';

    public $emailSubject = 'Tarjeta Personal Virtual';
}
