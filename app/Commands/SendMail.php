<?php

namespace App\Commands;

use CodeIgniter\CLI\BaseCommand;
use App\Models\Nomina;
use CodeIgniter\CLI\CLI;

class SendMail extends BaseCommand
{
    /**
     * The Command's Group
     *
     * @var string
     */
    protected $group = 'CodeIgniter';

    /**
     * The Command's Name
     *
     * @var string
     */
    protected $name = 'data:mail';

    /**
     * The Command's Description
     *
     * @var string
     */
    protected $description = 'Envía mail para los con los links';

    /**
     * The Command's Usage
     *
     * @var string
     */
    protected $usage = '';

    /**
     * The Command's Arguments
     *
     * @var array
     */
    protected $arguments = [];

    /**
     * The Command's Options
     *
     * @var array
     */
    protected $options = [];

    /**
     * Actually execute a command.
     *
     * @param array $params
     */
    public function run(array $params)
    {

        $outData = [];

        $outData[] = Nomina::getCSVHeaders();

        $cfg = Config('VCard');


        foreach ($this->getNomina($params) as $row) {
            $email = \Config\Services::email();
            $email->setFrom($cfg->mailFrom);
            $email->setSubject($cfg->emailSubject);
            $email->setMailType('html');
            if ($this->sendMail($row,$email,$cfg)) {
                $this->setNomina($row['id']);
                CLI::write('Mail enviado a ' . $row['x_email'] , 'green');
            }
            else {
                CLI::write('Error al enviar mail a ' . $row['x_email'] , 'light_red');
            }
        }
    }

    protected function sendMail($row, $email,$cfg) {
        $email->setTo($row['x_email']);

        $email->setMessage(view('email.php', [
            'persona' => $row['x_nombres'] . ' ' . $row['x_apellidos'],
            'link'    => $cfg->baseAppURL . '/' . $row['x_hash'],
        ]));

        return $email->send();


    }

    protected function getNomina($params)
    {
        return (new Nomina())->where(['m_sent' => 'N'])->findAll();
    }

    protected function setNomina($id)
    {
        (new Nomina())->update($id, ['m_sent' => 'S']);
    }
}
