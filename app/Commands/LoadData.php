<?php

namespace App\Commands;

use App\Models\Nomina;
use CodeIgniter\CLI\BaseCommand;

class LoadData extends BaseCommand
{
    /**
     * The Command's Group
     *
     * @var string
     */
    protected $group = 'CodeIgniter';

    /**
     * The Command's Name
     *
     * @var string
     */
    protected $name = 'data:load';

    /**
     * The Command's Description
     *
     * @var string
     */
    protected $description = 'Carga los datos en la base a partir de un csv ubicado en data/nomina.csv';

    /**
     * The Command's Usage
     *
     * @var string
     */
    protected $usage = 'data:load';

    /**
     * The Command's Arguments
     *
     * @var array
     */
    protected $arguments = [];

    /**
     * The Command's Options
     *
     * @var array
     */
    protected $options = [];

    /**
     * Actually execute a command.
     *
     * @param array $params
     */
    public function run(array $params)
    {
        if (($handle = fopen(FCPATH . "../data/nomina.csv", "r")) == false) {
            // TODO: error
        }
        // En la 1ra fila están los nombres de los campos
        if (($fields = fgetcsv($handle, null, ";")) == false) {
            // TODO: error
        }
        $model = new Nomina();
        while (($data = fgetcsv($handle, null, ";")) !== false) {
            try {
                $insData = [];
                $i=0;
                foreach ($fields as $field) {
                    $insData[$field] = $data[$i];
                    $i++;
                }
                $insData['m_sent'] = 'N';
                $user = $model->where(['n_legajo' => $insData['n_legajo']])->findAll();
                if ( count($user) == 0 )
                    $model->insert($insData);
                else 
                    $model->update($user[0]['id'],$insData);
            } catch (\Throwable $th) {
                throw $th;
            }
        }
        fclose($handle);
    }
}
