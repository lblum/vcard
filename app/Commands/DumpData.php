<?php

namespace App\Commands;

use CodeIgniter\CLI\BaseCommand;
use App\Models\Nomina;

class DumpData extends BaseCommand
{
    /**
     * The Command's Group
     *
     * @var string
     */
    protected $group = 'CodeIgniter';

    /**
     * The Command's Name
     *
     * @var string
     */
    protected $name = 'data:dump';

    /**
     * The Command's Description
     *
     * @var string
     */
    protected $description = 'Genera un CSV con los datos para la imprenta, y las imágenes de los QR';

    /**
     * The Command's Usage
     *
     * @var string
     */
    protected $usage = '';

    /**
     * The Command's Arguments
     *
     * @var array
     */
    protected $arguments = [];

    /**
     * The Command's Options
     *
     * @var array
     */
    protected $options = [];

    /**
     * Actually execute a command.
     *
     * @param array $params
     */
    public function run(array $params)
    {

        $outData = [];

        $outData[] = Nomina::getCSVHeaders();

        foreach ((new Nomina())->findAll() as $row) {
            $outData[] = $row;
        }
        
        
    }
}
