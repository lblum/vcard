<?php

namespace App\Controllers;

use App\Models\Nomina;
use chillerlan\QRCode\QRCode as QRCode;


class Home extends BaseController
{
    public function index($hash)
    {

        try {
            if (count(($user = (new Nomina())->where(['x_hash' => $hash])->findAll())) != 0) {
                // Añado la imagen del qr
                $persona = $user[0];
                $qrCode = new QRCode();
                $persona['x_vcard_img'] = $qrCode->render($persona['x_vcard']);

                return view(
                    'vcard',
                    [
                        "hash"    => $hash,
                        "persona" => json_encode($persona)
                    ]
                );
            } else {
                // TODO: HTTP_NOT_FOUND
                //return $this->respond([], ResponseInterface::HTTP_NOT_FOUND);
            }
        } catch (\Throwable $th) {
            // TODO: agregar log
            // TODO: HTTP_NOT_FOUND
            //return $this->respond([], ResponseInterface::HTTP_NOT_FOUND);
            throw $th;
        }




        return view('vcard', ["hash" => $hash]);
    }

    public function getJson($hash)
    {
        $arrayVar = [
            "name" => "Tarjeta Virtual",
            "short_name" => "Tarjeta Virtual",
            "start_url" => "/$hash",
            "display" => "standalone",
            "background_color" => "#ffffff",
            "theme_color" => "#219cdc",
            "description" => "Tarjeta Virtual",
            "orientation" => "portrait-primary",
            "prefer_related_applications" => false,
            "icons" => [
                [
                    "src" => "/icons/Icon-512.png",
                    "sizes" => "512x512 192x192 128x128 48x48",
                    "type" => "image/png",
                ],
            ],
        ];

        return $this->response->setJSON($arrayVar);
    }

    public function test($id){
        return view(
            'email',
            [
                "persona" => 'Test',
                'link' => 'https://www.menhir.com.ar',
            ]
        );
    }

}
