<?php

namespace App\Controllers;

use App\Models\Nomina;
use CodeIgniter\HTTP\ResponseInterface;

class VcardController extends BaseController
{
    /**
     * getVCF
     * 
     * Devuelve el contacto asociado a una tarjeta corporativa en formato vcf
     * En caso de que noexista, da 404
     *
     * @param string $hash
     * @return void
     */
    public function getVCF($hash) {
        try {
            if ( count(($user = (new Nomina())->where(['x_hash' => $hash ])->findAll())) != 0 ){
                // Añado la imagen del qr
                $retVal = $user[0];
                header('Access-Control-Allow-Origin: *');
                header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
                header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
                $this->response->setContentType('text/vcard');
        
                $stream = fopen('php://output', 'r+');
                fwrite($stream, $retVal['x_vcard']);
        
            } else {
                $this->response->setStatusCode(ResponseInterface::HTTP_NOT_FOUND,'');
            }
        } catch (\Throwable $th) {
            // TODO: agregar log
            $this->response->setStatusCode(ResponseInterface::HTTP_NOT_FOUND,'');
        }
    }

    public function index()
    {
    }
}
